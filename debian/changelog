gifsicle (1.93-2) unstable; urgency=medium

  * Upload to unstable.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Sun, 15 Aug 2021 11:47:36 +0200

gifsicle (1.93-1) experimental; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Wed, 30 Jun 2021 14:37:36 +0200

gifsicle (1.92-4) experimental; urgency=medium

  * d/watch: fixed. (Closes: #989397)

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Thu, 03 Jun 2021 13:20:48 +0200

gifsicle (1.92-3) experimental; urgency=medium

  * New Maintainer. Thanks Herbert Parentes Fortes Neto for the nice work.
    (Closes: #986934)
  * d/upstream/metadata: added.
  * Bump standards version to 4.5.1.
  * Bump debhelper version to 13.
  * d/control: added Rules-Requires-Root.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Wed, 02 Jun 2021 13:33:16 +0200

gifsicle (1.92-2) unstable; urgency=medium

  * Upload to unstable
  * debian/control:
      - Use 'debhelper-compat' rather than debian/compat file
      - Bump Standards-Version from 4.3.0 to 4.4.0

 -- Herbert Parentes Fortes Neto <hpfn@debian.org>  Tue, 23 Jul 2019 11:51:40 -0300

gifsicle (1.92-1) experimental; urgency=medium

  * New upstream version 1.92
  * DH_LEVEL: 12
  * debian/control:
      - Bump Standards-Version from 4.2.1 to 4.3.0
  * debian/copyright:
      - Update year: upstream, myself
  * debian/patches:
      - typo_manpage.patch

 -- Herbert Parentes Fortes Neto <hpfn@debian.org>  Thu, 30 May 2019 09:09:50 -0300

gifsicle (1.91-5) unstable; urgency=medium

  * debian/tests/control:
      - Add Depends: @builddeps@

 -- Herbert Parentes Fortes Neto <hpfn@debian.org>  Thu, 22 Nov 2018 08:32:53 -0200

gifsicle (1.91-4) unstable; urgency=medium

  * debian/control:
      - Bump Standards-Version from 4.1.4 to 4.2.1
  * debian/test/control:
      - Remove Depends
      - run autoreconf, configure, make check
  * debian/rules:
      - Remove trailing whitespaces
      - Run 'make check for tests
      - Remove '--with autoreconf'

 -- Herbert Parentes Fortes Neto <hpfn@debian.org>  Wed, 21 Nov 2018 09:56:07 -0200

gifsicle (1.91-3) unstable; urgency=medium

  * debian/control:
      - Bump Standards-Version from 4.1.3 to 4.1.4
  * debian/copyright:
      - Fix insecure-copyright-format-uri
  * debian/patches:
      - Add patch file to fix path in test files.
        new_path_to_exec_in_tests.patch
  * debian/rules:
      - Override_dh_auto_test to include upstream tests during build
  * debian/tests/: (created)
      - Add control file - CI

 -- Herbert Parentes Fortes Neto <hpfn@debian.org>  Sat, 30 Jun 2018 11:41:40 -0300

gifsicle (1.91-2) unstable; urgency=medium

  * I forgot to mention that this upstream version fixes
    important bugs:
    (Closes: #878739, #878736, #881141, #881120, #881119)

 -- Herbert Parentes Fortes Neto <hpfn@debian.org>  Tue, 09 Jan 2018 07:54:35 -0200

gifsicle (1.91-1) unstable; urgency=medium

  * New upstream version 1.91
  * DH_LEVEL: 11
  * debian/control:
      - Bump Standards-Version: 4.1.3
      - Update Vcs-* entries - salsa.debian.org
  * debian/copyright:
      - Update

 -- Herbert Parentes Fortes Neto <hpfn@debian.org>  Mon, 08 Jan 2018 14:55:35 -0200

gifsicle (1.90-1) unstable; urgency=medium

  * New upstream version 1.90
  * DH_LEVEL: 10

 -- Herbert Parentes Fortes Neto <hpfn@debian.org>  Sun, 20 Aug 2017 15:51:08 -0300

gifsicle (1.89-1) unstable; urgency=medium

  * New upstream version 1.89.
  * debian/control:
      - Bump STD-Version: from 3.9.8 to 4.0.0.
  * debian/copyright:
      - Update.
  * debian/patches:
      - Remove fix-escape-8bit-char.patch. Upstream fix.
  * debian/rules:
      - Rename NEWS -> NEWS.md.

 -- Herbert Parentes Fortes Neto <hpfn@debian.org>  Sun, 23 Jul 2017 15:17:39 -0300

gifsicle (1.88-3) unstable; urgency=medium

  * debian/rules:
      - Add '-fPIE -pie' to LDFLAGS.
      - Add '-fPIE' to CFLAGS.

 -- Herbert Parentes Fortes Neto <hpfn@debian.org>  Mon, 26 Dec 2016 11:54:57 -0200

gifsicle (1.88-2) unstable; urgency=medium

  * debian/control:
      - updated my email.
      - Bump Standards-Version from 3.9.6 to 3.9.8
      - updated Vcs-* entries. (https)
  * debian/copyright:
      - updated debian/* entry.
  * debian/patches:
      - fix-escape-8bit-char.patch created.
          Thanks Yuriy M. Kaminskiy - #838420.
  * debian/watch:
      - version 4

 -- Herbert Parentes Fortes Neto <hpfn@debian.org>  Mon, 26 Sep 2016 13:53:37 -0300

gifsicle (1.88-1) unstable; urgency=medium

  * New upstream release.
  * debian/copyright: updated.

 -- Herbert Parentes Fortes Neto <hpfn@ig.com.br>  Tue, 07 Jul 2015 15:33:10 -0300

gifsicle (1.87-2) unstable; urgency=medium

  * Upload to unstable.

 -- Herbert Parentes Fortes Neto <hpfn@ig.com.br>  Tue, 28 Apr 2015 11:43:24 -0300

gifsicle (1.87-1) experimental; urgency=medium

  * New upstream release.
  * New Maintainer. Thanks Francois Marier for the nice work. (Closes: #777102)
  * debian/control: Updated Vcs-Browser field.
  * debian/copyright updated.
  * debian/rules: Added hardening flags.
                  Added LDFLAGS. To avoid useless dependency.
  * debian/watch: Added an url option.
  * using dh-autoreconf instead of autotools-dev.

 -- Herbert Parentes Fortes Neto <hpfn@ig.com.br>  Tue, 24 Mar 2015 17:17:41 -0300

gifsicle (1.86-1) unstable; urgency=medium

  * New upstream release

 -- Francois Marier <francois@debian.org>  Wed, 15 Oct 2014 20:29:33 +1300

gifsicle (1.85-1) unstable; urgency=medium

  * New upstream release

 -- Francois Marier <francois@debian.org>  Wed, 15 Oct 2014 12:06:23 +1300

gifsicle (1.84-2) unstable; urgency=medium

  * Bump Standards-Version up to 3.9.6
  * Use more standard GPL short names in debian/copyright

 -- Francois Marier <francois@debian.org>  Thu, 18 Sep 2014 23:36:48 +1200

gifsicle (1.84-1) unstable; urgency=medium

  * New upstream release:
    - correct optimizer bug that affected GIFs with 65535+ colors

 -- Francois Marier <francois@debian.org>  Sat, 05 Jul 2014 21:18:48 +1200

gifsicle (1.83-1) unstable; urgency=medium

  * New upstream release:
    - correct bug in custom gamma values

 -- Francois Marier <francois@debian.org>  Tue, 22 Apr 2014 12:13:54 +1200

gifsicle (1.82-1) unstable; urgency=medium

  * New upstream release:
    - fix bug in "mix" sampling method

 -- Francois Marier <francois@debian.org>  Fri, 28 Mar 2014 12:18:41 +1300

gifsicle (1.81-1) unstable; urgency=medium

  * New upstream release:
    - fix bug in "mix" sampling method

 -- Francois Marier <francois@debian.org>  Thu, 27 Mar 2014 12:08:26 +1300

gifsicle (1.80-1) unstable; urgency=medium

  * New upstream release:
    - major improvements in image scaling
  * Disable upstream tests (not shipped with the tarball)

 -- Francois Marier <francois@debian.org>  Thu, 20 Mar 2014 18:58:39 +1300

gifsicle (1.78-1) unstable; urgency=medium

  * New upstream release:
    - correct optimization bug introduced in 1.76

 -- Francois Marier <francois@debian.org>  Tue, 10 Dec 2013 12:40:45 +1300

gifsicle (1.77-1) unstable; urgency=low

  * New upstream release:
    - major improvements to color selection
    - ordered dithering modes
    - halftone dithering
    - new `--fallback-delay` option

 -- Francois Marier <francois@debian.org>  Wed, 27 Nov 2013 13:26:58 +1300

gifsicle (1.76-1) unstable; urgency=low

  * New upstream release
  * Bump Standards-Version up to 3.9.5

 -- Francois Marier <francois@debian.org>  Mon, 25 Nov 2013 10:53:27 +1300

gifsicle (1.71-1) unstable; urgency=low

  * New upstream release:
    - now fully under GPLv2

  * Update debian/copyright to match upstream license changes
  * Relicense Gürkan's packaging under GPL2+ with his permission

  * Use canonical VCS URLs in debian/control
  * Update name of README in debian/docs
  * Properly install the upstream changelog

 -- Francois Marier <francois@debian.org>  Mon, 17 Jun 2013 12:34:48 +1200

gifsicle (1.70-1) unstable; urgency=low

  * New upstream release
  * Adopt this package (closes: #691822)

  * Host the packaging on git.debian.org
  * Switch to a minimal debian/rules file
  * Make debian/copyright machine-readable
  * Bump Standards-Version to 3.9.4
  * Bump debhelper compatibility to 9

 -- Francois Marier <francois@debian.org>  Thu, 16 May 2013 21:23:51 +1200

gifsicle (1.67-1) unstable; urgency=low

  * New upstream version.

 -- Gürkan Sengün <gurkan@phys.ethz.ch>  Tue, 08 May 2012 17:57:36 +0200

gifsicle (1.66-1) unstable; urgency=low

  * New upstream version.
  * Bump standards version to 3.9.3.
  * debian/copyright: Updated years.

 -- Gürkan Sengün <gurkan@phys.ethz.ch>  Wed, 04 Apr 2012 13:11:49 +0200

gifsicle (1.64-1) unstable; urgency=low

  * New upstream version.
  * Bump debhelper version to 8.

 -- Gürkan Sengün <gurkan@phys.ethz.ch>  Mon, 28 Nov 2011 10:31:52 +0100

gifsicle (1.63-1) unstable; urgency=low

  * New upstream version.
  * Bump standards version to 3.9.2.

 -- Gürkan Sengün <gurkan@phys.ethz.ch>  Thu, 04 Aug 2011 11:56:30 +0200

gifsicle (1.62-1) unstable; urgency=low

  * New upstream version.
    - Fixes csh specific manpage. (Closes: #606730)

 -- Gürkan Sengün <gurkan@phys.ethz.ch>  Thu, 14 Apr 2011 13:34:14 +0200

gifsicle (1.61-1) unstable; urgency=low

  * New upstream version.
    - Fixes assertion failure in optimize.c (LP: 647905)
  * Bump standards version to 3.9.1.

 -- Gürkan Sengün <gurkan@phys.ethz.ch>  Wed, 02 Mar 2011 11:47:18 +0100

gifsicle (1.60-1) unstable; urgency=low

  * New upstream version.
  * Switch to dpkg-source format version 3.0 (quilt).

 -- Gürkan Sengün <gurkan@phys.ethz.ch>  Thu, 15 Apr 2010 09:26:46 +0200

gifsicle (1.59-1) unstable; urgency=low

  * New upstream version.
  * Bump standards version to 3.8.4.

 -- Gürkan Sengün <gurkan@phys.ethz.ch>  Tue, 16 Mar 2010 09:24:56 +0100

gifsicle (1.58-1) unstable; urgency=low

  * New upstream version.
  * debian/copyright: update years.

 -- Gürkan Sengün <gurkan@phys.ethz.ch>  Wed, 20 Jan 2010 15:34:58 +0100

gifsicle (1.57-1) unstable; urgency=low

  * New upstream version.

 -- Gürkan Sengün <gurkan@phys.ethz.ch>  Thu, 12 Nov 2009 13:15:48 +0100

gifsicle (1.56-1) unstable; urgency=low

  * New upstream version.
  * debian/watch added.
  * Bump standards version.

 -- Gürkan Sengün <gurkan@phys.ethz.ch>  Mon, 19 Oct 2009 14:42:09 +0200

gifsicle (1.55-1) unstable; urgency=low

  * New upstream version.
  * Updated debian/copyright.
  * Bump standards version.
  * Bump debhelper version.

 -- Gürkan Sengün <gurkan@phys.ethz.ch>  Mon, 06 Apr 2009 15:59:08 +0200

gifsicle (1.51-1) unstable; urgency=low

  * New upstream version.

 -- Gürkan Sengün <gurkan@phys.ethz.ch>  Tue, 13 May 2008 14:24:41 +0200

gifsicle (1.49-1) unstable; urgency=low

  * New upstream version.
  * Updated my email address.
  * Moved homepage field.

 -- Gürkan Sengün <gurkan@phys.ethz.ch>  Mon, 05 May 2008 10:05:26 +0200

gifsicle (1.48-1) unstable; urgency=low

  * New upstream version.

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Mon, 19 Mar 2007 13:48:45 +0100

gifsicle (1.46-1) unstable; urgency=low

  * New upstream version.
  * Bump standards version, no changes needed.
  * Updated copyright.

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Mon, 15 Jan 2007 21:27:52 +0100

gifsicle (1.44-2) unstable; urgency=low

  * Update build-depends. (closes: #346590)
  * Apply patch to fix description. (closes: #345503)

  [Amaya Rodrigo]
  * Re-run autoconf.

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Wed, 11 Jan 2006 10:31:41 +0100

gifsicle (1.44-1) unstable; urgency=low

  * Initial release, done the package from scratch. (Closes: #344257)
  * Right into main. It used to be in non-free, woody (see #212193)

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Mon, 19 Dec 2005 15:15:41 +0100

gifsicle (1.23-2) unstable; urgency=low

  * Fixed build depends (closes: Bug#91299)

 -- root <root@macan.metainfo.org>  Tue, 27 Mar 2001 15:00:55 +0000

gifsicle (1.23-1) unstable; urgency=low

  * New upstream release

 -- Eduardo Marcel Macan <macan@debian.org>  Wed,  3 Jan 2001 21:08:12 -0200

gifsicle (1.17-1) unstable; urgency=low

  * Initial release.

 -- Eduardo Marcel Macan <macan@colband.com.br>  Thu, 23 Mar 2000 16:53:06 -0300
